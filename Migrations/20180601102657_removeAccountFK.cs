﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace survey.Migrations
{
    public partial class removeAccountFK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Submissions_Account_AccountID",
                table: "Submissions");

            migrationBuilder.DropIndex(
                name: "IX_Submissions_AccountID",
                table: "Submissions");

            migrationBuilder.DropColumn(
                name: "AccountID",
                table: "Submissions");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AccountID",
                table: "Submissions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Submissions_AccountID",
                table: "Submissions",
                column: "AccountID");

            migrationBuilder.AddForeignKey(
                name: "FK_Submissions_Account_AccountID",
                table: "Submissions",
                column: "AccountID",
                principalTable: "Account",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
