using survey.Models;
using Microsoft.EntityFrameworkCore;

namespace survey.Data {
    public class SurveyContext : DbContext {
        public SurveyContext(DbContextOptions<SurveyContext> options) : base(options){
        }

        public DbSet<Answer> Answers { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Survey> Surveys { get; set; }
        public DbSet<Submission> Submissions { get; set; }
        public DbSet<Account> Account { get; set; }
    }    
}