﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace survey.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
