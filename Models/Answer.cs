using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace survey.Models
{
    public class Answer
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int SubmissionID { get; set; }
        public int QuestionID { get; set; }

        public Submission Submission { get; set; }
        public Question Question { get; set; }
    }
}
