using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace survey.Models
{
    public class Survey
    {
        public int ID { get; set; }
        public string Desc { get; set; }

        public ICollection<Question> Questions { get; set; }
    }
}
