#Survey Web App
Web App to collect survey answers

Functional Requirements
	Admin
		Define survey
			Unique Name
			Label (question description)
			Answer (Text Field)
		View answered survey
			List 
				Survey name
				Name of user
				Date filled out
				Users answers
	User
		Answer survey
	
Non Functional Requirements
	No auth or roles
	No security
	Storage - Local or DB
	Tests
	Include package manager file
	Readme file
	Code annotations
	Rails/React or something else
	Deliver zip
	Deliver link to working instance
	Responsive
	Use backend and frontend frameworks
	
Entities:
	User
		ID
		Name
	Survey
		ID
		Name
	Question
		ID
		SurveyId
		QuestionText
	Submission
		ID
		UserId
		SurveyId
	Answer
		ID
		SubmissionId
		AnswerText
		
Pages
    Create Survey
    Take Survey
    View Surveys

API
    GET  /Survey (get survey list)
    POST /Survey (create survey)
    GET  /Survey/{ID} (get survey with questions)
    POST /Submission (Save survey answers)
    GET  /Submission (View submissions)
    GET  /Submission/{ID} (View submission answers)

Architecture
    database - AzureSql
    webserver - Web app
    backend - dotnet core
    frontend - react, bootstrap
